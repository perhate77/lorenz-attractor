//Lorenz system
/*
Lorenz Attractor
Some dynamical systems, like the one dimensional logistic map defined by x →
4x(1 − x), are chaotic everywhere, but in many cases chaotic behaviour is found
only in a subset of phase space. The cases of most interest arise when the chaotic
behaviour takes place on an attractor, since then a large set of initial conditions will
lead to orbits that converge to this chaotic region. An easy way to visualize a chaotic
attractor is to start with a point in the basin of attraction of the attractor, and then
simply plot its subsequent orbit. LorenzAttractor attractor results from a simple three-dimensional model of the Lorenz weather
system. The Lorenz attractor is perhaps one of the best known chaotic system
diagrams, probably because it was not only one of the first, but it is also one of
the most complex and as such gives rise to a very interesting pattern which looks
like the wings of a butterfly.
*/

#include<iostream>
#include<cmath>
#include<stdio.h> 
using namespace std;
int main()
{
	FILE *pFile;
	pFile=fopen("15.7.dat","w");
	int tmax; 
	float h,x,sig,b,r,xp,yp,zp,z,y,t;
	cout<<"enter the value of tmax\n=";
	cin>>tmax;
	cout<<"enter the value of h\n=";
	cin>>h;
	sig=10.0;
	r=28.0;
	b=8.0/3.0; 
	x=z=0.0;
	y=1.0;
	while(t<tmax)
	{
		fprintf(pFile,"%f %f\n",x,z);
		xp=sig*(y-x);
		yp=r*x-y-x*z;
		zp=x*y-b*z;
		x=x+h*xp;
		y+=h*yp;
		z+=h*zp;
		t+=h;
		cout<<x<<"  "<<z<<"\n";
	}
	fclose(pFile);
	return 0;
}


