**This is a serial code for Lorenz Attractor, written in C++.**

Compile using:


*   g++ LorenzAttractor.cpp



**Lorenz Attractor**

Some dynamical systems, like the one dimensional logistic map defined by x →
4x(1 − x), are chaotic everywhere, but in many cases chaotic behaviour is found
only in a subset of phase space. The cases of most interest arise when the chaotic
behaviour takes place on an attractor, since then a large set of initial conditions will
lead to orbits that converge to this chaotic region. An easy way to visualize a chaotic
attractor is to start with a point in the basin of attraction of the attractor, and then
simply plot its subsequent orbit. LorenzAttractor attractor results from a simple three-dimensional model of the Lorenz weather
system. The Lorenz attractor is perhaps one of the best known chaotic system
diagrams, probably because it was not only one of the first, but it is also one of
the most complex and as such gives rise to a very interesting pattern which looks
like the wings of a butterfly.


Ref.  _Computational Physics: An Introduction, R.C. Verma, etal. New Age International Publishers, New
Delhi(1999)_


![lorenz11](/uploads/26840db367b0f4951c0e6f112c023e16/lorenz11.png)





Written By::<br/>
Rajneesh Kumar<br/>

Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,<br/>
Bengaluru 560064, India.<br/>
Email: rajneesh[at]jncasr.ac.in<br/>
27 Dec, 2020
